<?php

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

function generateKey($length, $concat)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $concat . $randomString . rand(8, 3215);
}

function create(Model $model, $data)
{
    if ($model::create($data)) {
        return response()->json(array('Message' => "Créer avec succès !"), 200);
    } else {
        return response()->json(array('Message' => "Erreur d'enregistrement"));
    }
}

function show($data)
{
    return (is_null($data)) ? response()->json(["message" => "introuvable"]) : $data;
}

function AuthUser($user,  $next,  $request)
{
    $AuthUser = json_decode(json_encode(new UserResource(Auth::user())), true);
    $Authrole = $AuthUser['role'];
    if ($Authrole === $user) {
        return $next($request);
    } else {
        abort(response()->json(['message' => 'pas autorisé car vous n\'êtes pas un administrateur !!']));
    }
}
