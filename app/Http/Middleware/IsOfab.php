<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

class IsOfab
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $AuthUser = json_decode(json_encode(new UserResource(Auth::user())), true);
        $Authrole = $AuthUser['role'];
        if ($Authrole === "Ofab") {
            return $next($request);
        } else {
            abort(response()->json(['message' => 'pas autorisé car vous n\'êtes pas un administrateur de Orange Fab !!']));
        }
    }
}
