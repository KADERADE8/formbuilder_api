<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'role' => 'required',
            'password' => 'required|string|',
        ], $messages = [
            'required' => ':attribute est un champ obligatoire.',
            'max' => ':attribute ne doit pas etre superieur à :max chiffres',
            'between' => ':attribute doit etre entre :min et :max. ',
            'unique' => 'existe déja !'
        ]);
        if ($validator->fails()) {
            return response()->json(['Erreur de validation' => $validator->errors()]);
        }
        $role = Role::where('role', $request->role)->get();
        $roleId = json_decode($role)[0];

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        } elseif ($user) {
            $user->roles()->attach($roleId->id);
            return [
                'message' => 'Inscription effectué'
            ];
        }
    }

    public function login(Request $request)
    {

        $fields = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ], [
            'required' => ':attribute est un champ obligatoire.'
        ]);

        $user = User::where('email', $fields['email'])->first();

        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Identifiants incorrect !!'
            ], 401);
        }


        $token = $user->createToken('token')->plainTextToken;
        $role = json_decode($user->roles)[0]->role;
        $response = [
            "status" => "ok",
            "message" => "Connecté",
            "role" => $role,
            'token' => $token
        ];

        return response()
            ->json($response);
    }

    // method for user logout and delete token
    public function logout()
    {
        auth()->user()->tokens()->delete();
        return [
            'message' => 'Deconnecté'
        ];
    }
}
