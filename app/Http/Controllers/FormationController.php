<?php

namespace App\Http\Controllers;

use App\Models\Formation;
use Illuminate\Http\Request;
use App\Services\HashIdService;
use App\Http\Resources\FormationResource;
use Illuminate\Support\Facades\Validator;

class FormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()) {
            $userId = auth()->user()->id;
            $formationByUserId = Formation::where('id_user', '=', $userId)->get();
            $formation = FormationResource::collection($formationByUserId);
            return response()->json($formation);
        } else {
            return response()->json(FormationResource::collection(Formation::all()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = [
            "formation" => $request->formation,
            "id_formulaire" => (new HashIdService())->decode($request->id_formulaire),
            "id_user" => (new HashIdService())->decode($request->id_user),
            "description" => $request->description
        ];

        $validation = Validator::make($input, [
            "formation" => 'required',
            "id_formulaire" => 'required|exists:formulaires,id',
            "id_user" => 'required|exists:users,id',
            "description" => 'required'
        ], [
            "required" => ":attribute est un champs obligatoire",
            "exists" => "id_user introuvable"
        ]);

        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        if (Formation::create($input)) {
            return response()->json(array('Message' => "Créer avec succès !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Formation::find((new HashIdService())->decode($id));
        return (is_null($form)) ? response()->json(["message" => "introuvable"]) : response()->json(new FormationResource($form));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = Formation::find((new HashIdService())->decode($id));
        if (is_null($form)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            $input = [
                "formation" => $request->formation,
                "id_formulaire" => (new HashIdService())->decode($request->id_formulaire),
                "id_user" => (new HashIdService())->decode($request->id_user),
                "description" => $request->description
            ];
            $validation = Validator::make($input, [
                "formation" => 'required',
                "id_formulaire" => 'required|exists:formulaires,id',
                "id_user" => 'required|exists:users,id',
                "description" => 'required'

            ], [
                "required" => ":attribute est un champs obligatoire",
                "exist" => ":introuvable"
            ]);

            if ($validation->fails()) {
                return response()->json(['Erreur de validation' => $validation->errors()]);
            }

            if ($form->update($input)) {
                return response()->json(array('Message' => "Mis à jour !"), 200);
            } else {
                return response()->json(array('Message' => "Erreur d'enregistrement"));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Formation::find((new HashIdService())->decode($id));
        if (is_null($form)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            if ($form->delete()) {
                return response()->json(array('Message' => "Supprimée !"));
            } else {
                return response()->json(array('Message' => "Erreur"));
            }
        }
    }
}
