<?php

namespace App\Http\Controllers;

use App\Models\Formation;
use App\Models\Formulaire;
use App\Models\SoumttreFormulaire;
use App\Models\User;
use Illuminate\Http\Request;

class CountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = count(User::all());
        $formations = count(Formation::where('id_user', "=", auth()->user()->id)->get());
        $formulaires = count(Formulaire::where('id_user', "=", auth()->user()->id)->get());
        $soumissions = count(SoumttreFormulaire::whereRelation("formations", "id_user", "=", auth()->user()->id)->get());
        $counter = [
            "Utilisateurs" => $users,
            "Formations" => $formations,
            "Formulaires" => $formulaires,
            "Soumissions" => $soumissions,
        ];
        return response()->json($counter);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
