<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use App\Services\HashIdService;
use App\Http\Resources\PermisionResource;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(PermisionResource::collection(Permission::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validation = Validator::make($input, [
            "permissions" => 'required',
        ], [
            "required" => ":est un champs obligatoire",
        ]);

        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        if (Permission::create($input)) {
            return response()->json(array('Message' => "Créer avec succès !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $perm = new PermisionResource(Permission::find((new HashIdService())->decode($id)));
        return (is_null($perm)) ? response()->json(["message" => "introuvable"]) : response()->json($perm);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $perm = Permission::find((new HashIdService())->decode($id));
        if (is_null($perm)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            $input = $request->all();

            $validation = Validator::make($input, [
                "permissions" => 'required',
            ], [
                "required" => ":est un champs obligatoire",
            ]);

            if ($validation->fails()) {
                return response()->json(['Erreur de validation' => $validation->errors()]);
            }

            if ($perm->update($input)) {
                return response()->json(array('Message' => "Mis à jour !"), 200);
            } else {
                return response()->json(array('Message' => "Erreur d'enregistrement"));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $perm = Permission::find((new HashIdService())->decode($id));
        if (is_null($perm)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            if ($perm->delete()) {
                return response()->json(array('Message' => "Supprimée !"));
            } else {
                return response()->json(array('Message' => "Erreur"));
            }
        }
    }
}
