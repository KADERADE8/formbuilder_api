<?php

namespace App\Http\Controllers;

use App\Models\Formulaire;
use Illuminate\Http\Request;
use App\Services\HashIdService;
use App\Models\SoumttreFormulaire;
use App\Http\Resources\SubmitResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\FormulaireResource;

class FormulaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()) {
            $userId = auth()->user()->id;
            $formulaireByUserId = Formulaire::where('id_user', '=', $userId)->get();
            $formulaire = FormulaireResource::collection($formulaireByUserId);
            return response()->json($formulaire);
        } else {
            return response()->json(FormulaireResource::collection(Formulaire::all()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = [
            "nom_form" => $request->nom_form,
            "structure_form" => $request->structure_form,
            "visible" => $request->visible,
            "id_user" => (new HashIdService())->decode($request->id_user),
        ];

        $validation = Validator::make($input, [
            "nom_form" => 'required',
            "structure_form" => 'required',
            "visible" => 'required',
            "id_user" => 'required'
        ], ['required' => ':attribute est un champ obligatoire.', "exist" => ":introuvable"]);

        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        if (Formulaire::create($input)) {
            return response()->json(array('Message' => "Créer avec succès !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (auth()->user()) {
            $form = Formulaire::find((new HashIdService())->decode($id));
            return (is_null($form)) ? response()->json(["message" => "introuvable"]) : response()->json(new FormulaireResource($form));
        } else {
            $form = Formulaire::find((new HashIdService())->decode($id));
            return (is_null($form)) ? response()->json(["message" => "introuvable"]) : response()->json(new FormulaireResource($form));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = Formulaire::find((new HashIdService())->decode($id));
        if (is_null($form)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            $input = $request->all();
            $validation = Validator::make($input, [
                "nom_form" => 'required',
                "structure_form" => 'required',
                "visible" => 'required'
            ], ['required' => ':attribute est un champ obligatoire.', "exist" => ":introuvable"]);

            if ($validation->fails()) {
                return response()->json(['Erreur de validation' => $validation->errors()]);
            }

            if ($form->update($input)) {
                return response()->json(array('Message' => "Mis à jour !"), 200);
            } else {
                return response()->json(array('Message' => "Erreur d'enregistrement"));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Formulaire::find((new HashIdService())->decode($id));
        if (is_null($form)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            if ($form->delete()) {
                return response()->json(array('Message' => "Supprimée !"));
            } else {
                return response()->json(array('Message' => "Erreur"));
            }
        }
    }

    public function get()
    {
        $soumission = SoumttreFormulaire::whereRelation("formations", "id_user", "=", auth()->user()->id)->get();
        return response()->json(SubmitResource::collection($soumission));
    }

    public function submitById($id)
    {
        return response()->json(new SubmitResource(SoumttreFormulaire::findOrfail((new HashIdService())->decode($id))));
    }

    public function submit(Request $request)
    {
        $input = [
            "submit" => $request->submit,
            "id_formations" => (new HashIdService())->decode($request->id_formations),
            'status' => $request->status
        ];

        // $validation = Validator::make($input, [
        //     "submit" => 'required',
        //     "id_formations" => 'required|exists:formations,id',
        //     'status' => 'required'
        // ], ['required' => ':attribute est un champ obligatoire.']);

        // if ($validation->fails()) {
        //     return response()->json(['Erreur de validation' => $validation->errors()]);
        // }
        $submitForm = SoumttreFormulaire::create($input);
        if ($submitForm) {
            $submitForm->addToIndex();
            return response()->json(array('Message' => "Soumis avec succès !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }

    public function validationForm(Request $request, $id)
    {
        $submit = SoumttreFormulaire::findOrFail((new HashIdService())->decode($id));
        $input = $request->all();
        $validation = Validator::make($input, [
            'status' => 'required'
        ], ['required' => ':attribute est un champ obligatoire.']);

        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        if ($submit->update($input)) {
            return response()->json(array('Message' => "Mise à jour !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }
}
