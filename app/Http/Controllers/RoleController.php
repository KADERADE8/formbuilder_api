<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoleResource;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Services\HashIdService;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(RoleResource::collection(Role::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validation = Validator::make($input, [
            "role" => 'required',
        ], [
            "required" => ":est un champs obligatoire"
        ]);

        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        if (Role::create($input)) {
            return response()->json(array('Message' => "Créer avec succès !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find((new HashIdService())->decode($id));
        return (is_null($role)) ? response()->json(["message" => "introuvable"]) : response()->json(new RoleResource($role));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::find((new HashIdService())->decode($id));
        if (is_null($role)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            $input = $request->all();

            $validation = Validator::make($input, [
                "role" => 'required',
            ], [
                "required" => ":est un champs obligatoire"
            ]);

            if ($validation->fails()) {
                return response()->json(['Erreur de validation' => $validation->errors()]);
            }

            if ($role->update($input)) {
                return response()->json(array('Message' => "Mis à jour !"), 200);
            } else {
                return response()->json(array('Message' => "Erreur d'enregistrement"));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find((new HashIdService())->decode($id));
        if (is_null($role)) {
            return response()->json(["message" => "introuvable"]);
        } else {
            if ($role->delete()) {
                return response()->json(array('Message' => "Supprimée !"));
            } else {
                return response()->json(array('Message' => "Erreur"));
            }
        }
    }
}
