<?php

namespace App\Http\Resources;

use App\Services\HashIdService;
use Illuminate\Http\Resources\Json\JsonResource;

class PermisionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"=>(new HashIdService())->encode($this->id),
            "permissions"=>$this->permissions
        ];
    }
}
