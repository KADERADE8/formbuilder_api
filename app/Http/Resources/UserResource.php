<?php

namespace App\Http\Resources;

use App\Services\HashIdService;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => (new HashIdService())->encode($this->id),
            "username" => $this->name,
            "email" => $this->email,
            "fullname" => "",
            "role" => ($this->roles)[0]->role,
        ];
    }
}
