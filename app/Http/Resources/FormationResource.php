<?php

namespace App\Http\Resources;

use App\Services\HashIdService;
use Illuminate\Http\Resources\Json\JsonResource;

class FormationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => (new HashIdService())->encode($this->id),
            "formation" => $this->formation,
            "nom_form" => $this->formulaires->nom_form,
            "description" => $this->description,
            "id_formulaire" => (new HashIdService())->encode($this->formulaires->id),
            "formulaire" => $this->formulaires->structure_form,
            "Cree_par" => $this->users->name,
            "date_creation" => Date($this->created_at)
        ];
    }
}
