<?php

namespace App\Http\Resources;

use App\Services\HashIdService;
use Illuminate\Http\Resources\Json\JsonResource;

class FormulaireResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => (new HashIdService())->encode($this->id),
            "nom_form" => $this->nom_form,
            "structure_form" => $this->structure_form,
            "visible" => ($this->visible) ? "visible" : "invisible",
            "created_at" => Date($this->created_at)
        ];
    }
}
