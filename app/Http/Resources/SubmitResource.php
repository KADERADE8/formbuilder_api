<?php

namespace App\Http\Resources;

use App\Services\HashIdService;
use Illuminate\Support\Facades\Date;
use Illuminate\Http\Resources\Json\JsonResource;

class SubmitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => (new HashIdService())->encode($this->id),
            "submit" => $this->submit,
            "status" => ($this->status === 0) ? "non validé" : "validé",
            "id_formations" => $this->id_formations,
            "formations" => $this->formations->formation,
            "id_formulaire" => $this->formations->formulaires->id,
            "formulaire" => $this->formations->formulaires->nom_form,
            "created_at" => Date($this->created_at)
        ];
    }
}
