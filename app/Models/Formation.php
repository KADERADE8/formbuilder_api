<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;
    protected $fillable = ['formation', 'id_formulaire', 'id_user','description'];

    public function users()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function formulaires()
    {
        return $this->belongsTo(Formulaire::class, 'id_formulaire');
    }

    public function soumettre()
    {
        return $this->hasMany(SoumttreFormulaire::class, 'id_formations');
    }
}
