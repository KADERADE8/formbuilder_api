<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formulaire extends Model
{
    use HasFactory;

    protected $casts = [
        'structure_form' => 'array'
    ];

    protected $fillable = ['nom_form', 'structure_form', 'visible', 'id_user'];

    public function formations()
    {
        return $this->hasMany(Formation::class, 'id_formation');
    }

    public function soumettreformulaire()
    {
        return $this->hasMany(Formation::class, 'id_formulaire');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
