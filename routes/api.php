<?php

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\FormBuilder;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CountController;
use App\Http\Controllers\ElasticController;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\FormationController;
use App\Http\Controllers\FormulaireController;
use App\Http\Controllers\PermissionController;
use App\Models\SoumttreFormulaire;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResource('/Forms', FormulaireController::class);

// Route::post('/addRole',RoleController::class);

// Route::post('/addPermission',PermissionController::class);


// //ODA
// Route::group(['middleware' => ['auth:sanctum', 'IsODA']], function () {

//     Route::get('/profile', function () {
//         return json_decode(json_encode(new UserResource(Auth::user())), true);
//     });

//     Route::get('/FormationODA', [FormationController::class, 'index']);

// });

// //Orange Fab
// Route::group(['middleware' => ['auth:sanctum', 'IsOfab']], function () {
//     Route::get('/profile', function () {
//         return json_decode(json_encode(new UserResource(Auth::user())), true);
//     });

//     Route::get('/FormationOfab', [FormationController::class, 'index']);

// });

// //Fab lab
// Route::group(['middleware' => ['auth:sanctum', 'IsFabLab']], function () {
//     Route::get('/profile', function () {
//         return json_decode(json_encode(new UserResource(Auth::user())), true);
//     });

// });

Route::post('/register', [AuthController::class, 'register']);

Route::post('/login', [AuthController::class, 'login']);

Route::apiResource('/Users', UserController::class);


//Administrateur
Route::group([
    'middleware' => ['auth:sanctum']
], function () {

    Route::apiResource('/permissions', PermissionController::class);

    Route::apiResource('/roles', RoleController::class);

    Route::apiResource('/formations', FormationController::class);

    Route::apiResource('/Formulaires', FormulaireController::class);

    Route::post('/EnregistrerFormulaire', [FormulaireController::class, 'store']);

    Route::post('/SoumettreFormulaire', [FormulaireController::class, 'submit']);

    Route::get('/SoumettreFormulaire', [FormulaireController::class, 'get']);

    Route::get('/SoumettreFormulaire/{id}', [FormulaireController::class, 'submitById']);

    Route::put('/Validation/{id}', [FormulaireController::class, 'validationForm']);

    Route::get('/Counter', [CountController::class, 'index']);

    Route::get('/Search/{params?}', [ElasticController::class, 'index']);
    Route::get('/profile', function () {
        return json_decode(json_encode(new UserResource(Auth::user())), true);
    });
    Route::apiResource('/Formation', FormationController::class);
    Route::apiResource('/Formulaire', FormulaireController::class);
});
